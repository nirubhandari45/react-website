  
import React from "react";


const Navbar = () => {
 
  return (
    <>
      <header>
        <div className="continue-shopping">
          <img src="../image/cart.jpg" alt="arrow" className="arrow-icon" />
          <h3>continue shoping</h3>
        </div>

        <div className="cart-icon">
          <img src="../image/cart.jpg" alt="cart-logo" />
          <p>total</p>
        </div>
      </header>
    </>
  );
};

export default Navbar;